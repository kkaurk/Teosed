var allArtWorkList;
var urlTxtAWT = "http://localhost:8080/rest/proov/artwork_types";
var conTextAWT = "application/json; charset=utf-8";

function getAllArtWorkTypesAndFillTable() {
    $.getJSON(urlTxtAWT, function (allArtWorkTypes) {
            allArtWorkList = allArtWorkTypes;
            <!-- see siin on tegelikult globaalse muutuja väärtustamine. Globaalset muutujat läheb vaja, et mujal sama Jsonit kasutada -->
            var tableBodyContent = "";
            for (var i = 0; i < allArtWorkTypes.length; i++) {
                tableBodyContent = tableBodyContent +
                    "<tr><td>" + allArtWorkTypes[i].typeId +
                    "</td><td>" + allArtWorkTypes[i].typeName +
                    "</td><td>" + allArtWorkTypes[i].typeDescription +
                    "</td><td><button type='button' onclick='deleteArtWorkType(" + allArtWorkTypes[i].typeId + ")'>Delete</button>" +
                    "<button type='button' onclick='modifyCourseType(" + allArtWorkTypes[i].typeId + ")'>Modify</button>" +
                    "</td></tr>"
            }
            document.getElementById("ArtworkTypesTableBody").innerHTML = tableBodyContent;
        }
    );
}

getAllArtWorkTypesAndFillTable();

function addNewArtWorkType() {
    var jsonData = {
        "typeDescription": document.getElementById("newArtWorkTypeDescription").value,
        "typeName": document.getElementById("newArtWorkTypeName").value
    };
    var jsonString = JSON.stringify(jsonData);

    $.ajax({
        url: urlTxtAWT,
        type: "POST",
        data: jsonString,
        contentType: conTextAWT,
        success: function () {
            document.getElementById("newArtWorkTypeDescription").value = "";
            document.getElementById("newArtWorkTypeName").value = "";
            document.getElementById("modifyArtWorkTypeId").value = "";
            document.getElementById("modifyArtWorkTypeName").value = "";
            document.getElementById("modifyArtWorkTypeDescription").value = "";
            getAllArtWorkTypesAndFillTable();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on adding new teose tüüp");
        }
    })
}

function deleteArtWorkType(artWorkTypeId) {
    var jsonData = {
        "typeId": artWorkTypeId
    };
    var jsonDataString = JSON.stringify(jsonData);
    $.ajax({
        url: urlTxtAWT,
        type: "DELETE",
        contentType: conTextAWT,
        data: jsonDataString,
        success: function () {
            getAllArtWorkTypesAndFillTable();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on deleting artworktype");
        }
    })


}

function modifyCourseType(artWorkTypeId) {
    for (var i = 0; i < allArtWorkList.length; i++) {
        if (allArtWorkList[i].typeId == artWorkTypeId) {
            document.getElementById("modifyArtWorkTypeId").value = artWorkTypeId;
            document.getElementById("modifyArtWorkTypeName").value = allArtWorkList[i].typeName;
            document.getElementById("modifyArtWorkTypeDescription").value = allArtWorkList[i].typeDescription;
        }
    }

}

function saveArtWorkTypeChanges() {
    var jsonData = {
        "typeId": document.getElementById("modifyArtWorkTypeId").value,
        "typeName": document.getElementById("modifyArtWorkTypeName").value,
        "typeDescription": document.getElementById("modifyArtWorkTypeDescription").value
    };
    var jsonDataString = JSON.stringify(jsonData);
    var urlTxtAwtForChange = urlTxtAWT + "/" + "typeId"
    $.ajax({
        url: urlTxtAwtForChange,
        type: "PUT",
        data: jsonDataString,
        contentType: conTextAWT,
        success: function () {
            document.getElementById("newArtWorkTypeDescription").value = "";
            document.getElementById("newArtWorkTypeName").value = "";
            document.getElementById("modifyArtWorkTypeId").value = "";
            document.getElementById("modifyArtWorkTypeName").value = "";
            document.getElementById("modifyArtWorkTypeDescription").value = "";
            getAllArtWorkTypesAndFillTable()
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on modifying teose tüüp")
        }
    })

}

