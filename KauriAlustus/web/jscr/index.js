var allArtworkList;
var urlTextArtworks = "http://localhost:8080/rest/proov/artwork_units";
var conText = "application/json; charset=utf-8";
var artWorkTypesForList;
var allRolesForList;
var allAuthorsForList;
var artWorkIdGlobal;

// järgnevad muutujad on id-de nimed millele vajutades hakkab vastav lisa
// (datepicker) tööle. Ühtlasi ei lase sisestada muid väärtusi kui tuleb valikust


$(document).ready(function () {
    $("#newArtworkCreation").datepicker({dateFormat: "dd.mm.yy"});
    $("#newArtworkPresentation").datepicker({dateFormat: "dd.mm.yy"});
    $("#modifyArtworkCreationTime").datepicker({dateFormat: "dd.mm.yy"});
    $("#modifyArtworkPresentation").datepicker({dateFormat: "dd.mm.yy"});
});

function getAllArtworks() {
    $.getJSON("http://localhost:8080/rest/proov/artworkunits_view", function (allArtworksData) {
        allArtworkList = allArtworksData;
        var artworksTableBody = "";
        var radioId = "";
        for (var i = 0; i < allArtworksData.length; i++) {
            radioId = "awradio" + i;
            artworksTableBody = artworksTableBody +
                "<tr><td><a href= '../artworkdetails.html?" + allArtworksData[i].artworkIdV + "'>" + allArtworksData[i].artworkNameV + "</a>" +
                "</td><td><input type='radio' name = 'awUnitId' value = '" +
                allArtworksData[i].artworkIdV + "' id = '" + radioId + "'>" +
                "</td><td>" + allArtworksData[i].artworkTimeV +
                "</td><td>" + allArtworksData[i].artworkPresentationV +
                "</td><td>" + allArtworksData[i].artworkPresentatinV +
                "</td><td>" + allArtworksData[i].artworkTypeNameV +
                "</td><td id='awCell" + allArtworksData[i].artworkIdV + "'>" +
                "</td></tr>";
        }
        document.getElementById("ArtworksData").innerHTML = artworksTableBody;
    })
}

getAllArtworks();

function fillAuthorsCells() {

    $.getJSON("http://localhost:8080/rest/proov/artwork_authors_view").then(function(awAuthorsData) {
        var tabelAuthorContent = "";
        var second = false;
        var awIdToCompare = "";
        for (var i = 0; i < awAuthorsData.length; i++) {
            if (awIdToCompare !== awAuthorsData[i].artworkIdV) {
                if (awIdToCompare !=="") {
                    document.getElementById("awCell" + awIdToCompare).innerHTML = tabelAuthorContent;
                }
                tabelAuthorContent = "";
                second = false;
            }
            if (second === true) {
                tabelAuthorContent = tabelAuthorContent + "<br>";
            }
            tabelAuthorContent = tabelAuthorContent + awAuthorsData[i].roleNameV + ": " +
                awAuthorsData[i].firstNameV + " " + awAuthorsData[i].lastNameV;
            second = true;
            awIdToCompare = awAuthorsData[i].artworkIdV;
        }
        document.getElementById("awCell" + awIdToCompare).innerHTML = tabelAuthorContent;
    })
}

fillAuthorsCells();

function typesOfArtworks() {
    var myDiv = document.getElementById("artWorkTypesList");
    $.getJSON("http://localhost:8080/rest/proov/artwork_types", function (allArtworkTypesForList) {
        artWorkTypesForList = allArtworkTypesForList;
        var selectList = document.createElement("select");
        selectList.setAttribute("id", "mySelect");
        myDiv.appendChild(selectList);
        for (var i = 0; i < allArtworkTypesForList.length; i++) {
            var stylesOption = document.createElement("option");
            stylesOption.setAttribute("value", allArtworkTypesForList[i].typeId);
            stylesOption.text = allArtworkTypesForList[i].typeName;
            selectList.appendChild(stylesOption);
        }
    })
}

typesOfArtworks();

function typesOfArtworksForChange(receivedId) {
    var myDiv1 = document.getElementById("artWorkTypesList2");
    var selectList1 = document.createElement("select");
    selectList1.setAttribute("id", "mySelect1");
    myDiv1.appendChild(selectList1);
    for (var i = 0; i < artWorkTypesForList.length; i++) {
        var stylesOption1 = document.createElement("option");
        stylesOption1.setAttribute("value", artWorkTypesForList[i].typeId);
        stylesOption1.text = artWorkTypesForList[i].typeName;
        if (artWorkTypesForList[i].typeName === receivedId) {
            var selectedItem = i;
        }
        selectList1.appendChild(stylesOption1);
    }
    document.getElementById("mySelect1").selectedIndex = selectedItem;
}


function saveArtwork() {
    var parts1 = document.getElementById("newArtworkCreation").value.split('.');
    var firstDate = parts1[2] + "/" + parts1[1] + "/" + parts1[0];
    var parts2 = document.getElementById("newArtworkPresentation").value.split('.');
    var secondDate = parts2[2] + "/" + parts2[1] + "/" + parts2[0];
    var listElementValue1 = document.getElementById("mySelect");
    var listElementValue2 = listElementValue1.value;
    var jsonDataAdd = {
        "artworkName": document.getElementById("newArtworkName").value,
        "artworkCreationTime": firstDate,
        "artworkFirstPresentation": secondDate,
        "placeOfFirstPresentation": document.getElementById("newArtworkPresentPlace").value,
        "typeId": listElementValue2
    };
    var jsonDataAddString = JSON.stringify(jsonDataAdd);
    $.ajax({
        url: urlTextArtworks,
        type: "POST",
        data: jsonDataAddString,
        contentType: conText,
        success: function () {
            getAllArtworks();
            fillAuthorsCells();
//            emptyDataLabels();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("Eroor on adding artwork");
        }
    });
    document.getElementById("overlayForInput").style.display = "none";
}


function changeArtwork() {
    var txtMy = "";
    var awRadioName = "";
    var idToControl;
    var isChecked = false;
    for (var i = 0; i < allArtworkList.length; i++) {
        awRadioName = "awradio" + i;
        if (document.getElementById(awRadioName).checked) {
            idToControl = document.getElementById(awRadioName).value;
            document.getElementById("modifyArtworId").value = idToControl;
            document.getElementById("modifyArtworkName").value = allArtworkList[i].artworkNameV;
            document.getElementById("modifyArtworkCreationTime").value = allArtworkList[i].artworkTimeV;
            document.getElementById("modifyArtworkPresentation").value = allArtworkList[i].artworkPresentationV;
            document.getElementById("modifyArtworkPresentPlace").value = allArtworkList[i].artworkPresentatinV;
            typesOfArtworksForChange(allArtworkList[i].artworkTypeNameV);
            txtMy = txtMy + idToControl + " ;";
            isChecked = true;
        }
    }
    if (isChecked === true) {
        document.getElementById("overlayForChange").style.display = "block";
    } else {
        alert("Muudetav teos on valimata");
    }
}

function saveArtworkDataChanges() {
    var parts11 = document.getElementById("modifyArtworkCreationTime").value.split('.');
    var firstDate1 = parts11[2] + "/" + parts11[1] + "/" + parts11[0];
    var parts21 = document.getElementById("modifyArtworkPresentation").value.split('.');
    var secondDate1 = parts21[2] + "/" + parts21[1] + "/" + parts21[0];
    var listElementValue1 = document.getElementById("mySelect1");
    var listElementValue2 = listElementValue1.value;
    var jsonUpdateData = {
        "artworkID": document.getElementById("modifyArtworId").value,
        "artworkName": document.getElementById("modifyArtworkName").value,
        "artworkCreationTime": firstDate1,
        "artworkFirstPresentation": secondDate1,
        "placeOfFirstPresentation": document.getElementById("modifyArtworkPresentPlace").value,
        "typeId": listElementValue2
    };
    var jsonDataString = JSON.stringify(jsonUpdateData);
    var urlWithParam = urlTextArtworks + "/artworkID";
    $.ajax({
        url: urlWithParam,
        type: "PUT",
        data: jsonDataString,
        contentType: conText,
        success: function () {
            emptyDataLabels();
            getAllArtworks();
            fillAuthorsCells();
        }, error: function (XMLHTTPRequest, textStatus, errorThrown) {
            alert("Andmed uuendamata");
            console.log("Error on modifying data");
        }
    });
    document.getElementById("overlayForChange").style.display = "none";
}

function addAuthorToArtwork() {
    var awRadioName = "";
    var addAuthorTableBody = "";
    var isAddChecked = false;
    for (var i = 0; i < allArtworkList.length; i++) {
        awRadioName = "awradio" + i;
        if (document.getElementById(awRadioName).checked) {
            artWorkIdGlobal = document.getElementById(awRadioName).value;
            addAuthorTableBody = addAuthorTableBody +
                "<tr><td>" + allArtworkList[i].artworkNameV +
                "</td><td>" + allArtworkList[i].artworkTimeV +
                "</td><td>" + allArtworkList[i].artworkPresentationV +
                "</td><td>" + allArtworkList[i].artworkPresentatinV +
                "</td><td>" + allArtworkList[i].artworkTypeNameV +
                "</td></tr>";
            isAddChecked = true;
        }
        document.getElementById("AddAuthorToArtwork").innerHTML = addAuthorTableBody;
    }
    if (isAddChecked === true) {
        typesOfRoles();
        authorsToChoose();
        document.getElementById("overlayForAddAuthor").style.display = "block";
    } else {
        alert("Teos, millele autorit lisada on valimata");
    }

}

function typesOfRoles() {
    if (document.getElementById("roleSelect")) {

    } else {
        var divForRoleList = document.getElementById("authorsTypeSelect");
        $.getJSON("http://localhost:8080/rest/proov/roles", function (allRolesList) {
            allRolesForList = allRolesList;
            var selectRoleList = document.createElement("select");
            selectRoleList.setAttribute("id", "roleSelect");
            divForRoleList.appendChild(selectRoleList);
            for (var i = 0; i < allRolesList.length; i++) {
                var roleStylesOption = document.createElement("option");
                roleStylesOption.setAttribute("value", allRolesList[i].roleId);
                roleStylesOption.text = allRolesList[i].roleName;
                selectRoleList.appendChild(roleStylesOption);
            }
        })
    }
}

function authorsToChoose() {
// teha selgeks, mille järgi saab datalistis kontrollida, kas see on juba tehtud ja
// see kontroll peale panna
    var authorDatalist = document.getElementById("selectAuthor");
    $.getJSON("http://localhost:8080/rest/proov/author", function (allAuthorsList) {
        allAuthorsForList = allAuthorsList;
        for (var i = 0; i < allAuthorsList.length; i++) {
            var authorsOption = document.createElement("option");
            authorsOption.value = (allAuthorsList[i].lastName + ", " + allAuthorsList[i].firstName);
            authorDatalist.appendChild(authorsOption);
        }
    })
}

function saveAuthorsData() {
    // Siin on tegemata osa kui sisestatakse autor, keda listis pole
    var authorType = document.getElementById("roleSelect").value;
    var authorFullName = document.getElementById("selectOfAuthors").value;
    var nameParts = authorFullName.split(", ");
    var authorLastName = nameParts[0];
    var authorFirstName = nameParts[1];
    for (i = 0; i < allAuthorsForList.length; i++) {
        if (allAuthorsForList[i].lastName == authorLastName && allAuthorsForList[i].firstName == authorFirstName) {
            var authorIdForSend = allAuthorsForList[i].authorId;
        }
    }
    var jsonForRoles = {
        "artworkIdAa": artWorkIdGlobal,
        "authorIdAa": authorIdForSend,
        "roleIdAa": authorType
    };
    var jsonRolesString = JSON.stringify(jsonForRoles);
    $.ajax({
        url: "http://localhost:8080/rest/proov/artwork_authors",
        type: "POST",
        data: jsonRolesString,
        contentType: conText,
        success: function () {
            getAllArtworks();
            fillAuthorsCells();
//            emptyDataLabels();
        }, error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("Error on adding author to artwork");
        }
    });
    document.getElementById("overlayForAddAuthor").style.display = "none";
}

function deleteArtwork() {
    var awRadioName = "";
    var idToDelete;
    var isChecked = false;
    for (var i = 0; i < allArtworkList.length; i++) {
        awRadioName = "awradio" + i;
        if (document.getElementById(awRadioName).checked) {
            idToDelete = document.getElementById(awRadioName).value;
            var jsonData = {
                "artworkID": idToDelete
            };
            var jsonDataString = JSON.stringify(jsonData);
            $.ajax({
                url: urlTextArtworks,
                type: "DELETE",
                data: jsonDataString,
                contentType: conText,
                success: function (){
                    getAllArtworks();
                    fillAuthorsCells();
                }, error: function(XMLHTTPRequest, textStatus, errorThrown){
                    console.log("Error on deleting artwork");
                }
            });
            isChecked = true;
        }
    }
    if (isChecked === false) {
        alert("Muudetav teos on valimata");
    }
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

function emptyDataLabels() {
    document.getElementById("newArtworkName").value = "";
    document.getElementById("newArtworkCreation").value = "";
    document.getElementById("newArtworkPresentation").value = "";
    document.getElementById("newArtworkPresentPlace").value = "";
    document.getElementById("modifyArtworId").value = "";
    document.getElementById("modifyArtworkName").value = "";
    document.getElementById("modifyArtworkCreationTime").value = "";
    document.getElementById("modifyArtworkPresentation").value = "";
    document.getElementById("modifyArtworkPresentPlace").value = "";
}

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

function overlayOn() {
    document.getElementById("overlayForInput").style.display = "block";
}

function backFromChangeOverlay() {
    document.getElementById("overlayForChange").style.display = "none";
}

function backFromAddOverlay() {
    document.getElementById("overlayForInput").style.display = "none";
}

function backFromaddAuthorOverlay() {
    document.getElementById("overlayForAddAuthor").style.display = "none";
}