package resources.utils;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public abstract class ResourcesUtils {
    public static java.sql.Date javaDateToSqlDate(Date dateToConvert){
        return new java.sql.Date(dateToConvert.getTime());
    }

    public static java.util.Date sqlDateToJavaDate(java.sql.Date dateToConvert){
        java.util.Date javaDate=null;
        if (dateToConvert != null){
            javaDate = new Date(dateToConvert.getTime());
            return javaDate;
        }
        return null;
    }

    public static String sqlDateToString(java.sql.Date date){
        if(date != null) {
            java.util.Date utilDate = new java.util.Date(date.getTime());
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
            return dateFormat.format(utilDate);
        }
        return null;
    }
}
