CREATE TABLE `artwork_units` (
  `artwork_id` INT(9) UNSIGNED NOT NULL AUTO_INCREMENT,
  `artwork_name` varchar(90) NOT NULL,
  `artwork_creation_time` date DEFAULT NULL,
  `first_presentation` date DEFAULT NULL,
  `place_of_first_presentatin` varchar(90) DEFAULT NULL,
  `Type_ID` int(2) UNSIGNED NOT NULL,
  PRIMARY KEY (`artwork_id`),
  KEY `type_id_idx` (`Type_ID`),
  CONSTRAINT `type_id` FOREIGN KEY (`Type_ID`) REFERENCES `artwork_types` (`type_id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Different artworks, names, authors and others';