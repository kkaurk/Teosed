CREATE TABLE `texts_points` (
	`texts_points_id` INT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    `text_id`INT(6) UNSIGNED NOT NULL,
    `start_point` INT(5) UNSIGNED NOT NULL,
    `end_point`INT(5) UNSIGNED NOT NULL,
    PRIMARY KEY (`texts_points_id`),
    KEY `text_id_idx`(`text_id`),
    CONSTRAINT `text_id` FOREIGN KEY(`text_id`) REFERENCES `artwork_texts`(`text_id`) ON UPDATE NO ACTION
);