var curAwId;
var conText = "application/json; charset=utf-8";
var allArtworkListInPf;


function getCurrentArtworkId() {
    var parts = document.URL.split("?");
    curAwId = parts[1];
}

getCurrentArtworkId();

function getCurrentArtwork() {
    $.getJSON("http://localhost:8080/rest/proov/artworkunits_view/artworkIdV/" + curAwId, function (currentArtworkData) {
        var curArtworkTableBodz = "";
        for (var i = 0; i < currentArtworkData.length; i++) {
            curArtworkTableBodz = curArtworkTableBodz +
                "<tr><td>" + currentArtworkData[i].artworkNameV +
                "</td><td>" + currentArtworkData[i].artworkTimeV +
                "</td><td>" + currentArtworkData[i].artworkPresentationV +
                "</td><td>" + currentArtworkData[i].artworkPresentatinV +
                "</td><td>" + currentArtworkData[i].artworkTypeNameV +
                "</td></tr>";
        }
        document.getElementById("awBaseData").innerHTML = curArtworkTableBodz;
    })
}

getCurrentArtwork();

function getCurrentArtworkAuthors() {
    var currentAuthorTable = "";
    $.getJSON("http://localhost:8080/rest/proov/artwork_authors_view/artworkIdV/" + curAwId).then(function (currentAuthorsData) {
        if (currentAuthorsData.length !== 0) {
            currentAuthorTable = "<thead><tr><th>Mille" +
                " autor</th><th>Nimi</th></tr></thead><tbody>";
            for (var i = 0; i < currentAuthorsData.length; i++) {
                currentAuthorTable = currentAuthorTable +
                    "<tr><td>" + currentAuthorsData[i].roleNameV +
                    "</td><td>" + currentAuthorsData[i].firstNameV + " " + currentAuthorsData[i].lastNameV +
                    "</td></tr>";
            }
            currentAuthorTable = currentAuthorTable + "</tbody><hr/>";
            document.getElementById("curAwAuthors").innerHTML = currentAuthorTable;
        }
    })
}

getCurrentArtworkAuthors();

function getCurrentArtworkPerformances() {
    var currentPerformanceTable = "";
    $.getJSON("http://localhost:8080/rest/proov/aw_pf_byid/artworkIdV/" + curAwId).then(function (performancesData) {
        if (performancesData.length !== 0) {
            currentPerformanceTable = currentPerformanceTable + "<thead><tr><th>Koosseisud</th></tr></thead><tbody>";
            for (var i = 0; i < performancesData.length; i++) {
                currentPerformanceTable = currentPerformanceTable +
                    "<tr><td>" + performancesData[i].performanceNameV +
                    "</td></tr>";
            }
            currentPerformanceTable = currentPerformanceTable + "</tbody><hr/>";
            document.getElementById("curAwPerformances").innerHTML = currentPerformanceTable;
        }
    })
}

getCurrentArtworkPerformances();

function getCurrentArtworkUses() {
    var awUsesTable = "";
    $.getJSON("http://localhost:8080/rest/proov/aw_uses_aw/parentArtworkV/" + curAwId).then(function (awUsesData) {
        if (awUsesData.length !== 0){
            awUsesTable = awUsesTable + "<thead><tr><th>Käesolev" +
                " teos kasutab alljärgnevaid teoseid</th></tr></thead><tbody>";
            for (var i=0; i<awUsesData.length; i++){
                awUsesTable = awUsesTable +
                    "<tr><td><a href= '../artworkdetails.html?" + awUsesData[i].childArtworkV + "'>" +
                    awUsesData[i].artworkNameV + "</a>" +
                    "</td><td>" + awUsesData[i].typeNameV;
                    "</td></tr>";
            }
            awUsesTable = awUsesTable + "</tbody><hr/>";
            document.getElementById("awUsesAws").innerHTML = awUsesTable;
        }

    })
}

getCurrentArtworkUses();

function addPerformanceToArtwork() {
    document.getElementById("perfMessage").innerHTML = " Vali koosseis";
    doPfList();
    document.getElementById("pfSave").innerHTML = "<button type='button'" +
        " onclick='savePfData()'>Salvesta koosseis</button>"

}

function awUsesAw() {
    document.getElementById("artworksListLabel").innerHTML = "Vali teos, mida" +
        " kasutatakse";
    var divForPfDataInput = document.getElementById("pfInput");
    var pfSelectDataList = document.createElement("input");
    pfSelectDataList.setAttribute("list", "selectPf");
    pfSelectDataList.setAttribute("name", "selectPf");
    pfSelectDataList.setAttribute("id", "selectPfs");
    divForPfDataInput.appendChild(pfSelectDataList);
//    var pfDataList = document.getElementById("datalist");
    var pfDatalist = document.createElement("datalist");
    pfDatalist.setAttribute("id", "selectPf");
    pfSelectDataList.appendChild(pfDatalist);
    $.getJSON("http://localhost:8080/rest/proov/artworkunits_view").then(function (artworksData) {
        allArtworkListInPf = artworksData;
        for (var i = 0; i < artworksData.length; i++) {
            var pfOption = document.createElement("option");
            pfOption.value = artworksData[i].artworkNameV;
            pfDatalist.appendChild(pfOption);
        }
    });
    document.getElementById("awSave").innerHTML = "<button type='button'" +
        " onclick='saveAwUsageData()'>Salvesta kasutatav teos</button>";
}

function saveAwUsageData() {
    var first = true;
    for (var i=0; i<allArtworkListInPf.length; i++){
        if (document.getElementById("selectPfs").value == allArtworkListInPf[i].artworkNameV && first == true ){
            var jsonAwUsageData = {
                "parentArtwork": curAwId,
                "childArtwork": allArtworkListInPf[i].artworkIdV
            };
            var jsonAwUsageString = JSON.stringify(jsonAwUsageData);
            $.ajax({
                url: "http://localhost:8080/rest/proov/artwork_usage",
                type: "POST",
                data: jsonAwUsageString,
                contentType: conText,
                success: function () {
                    getCurrentArtworkUses();
                }
            });
            first = false;
        }
    }
    if (first == true){
        alert("Autor tuleb valide nimekirjast. Kui autor puudub, lisa enne" +
            " autorite osas uus autor.");
    }
    document.getElementById("artworksListLabel").innerHTML = "";
    document.getElementById("pfInput").innerHTML = "";
    document.getElementById("awSave").innerHTML = "";
}


function doPfList() {
    if (document.getElementById("pfSelect")) {

    } else {
        var divForPfList = document.getElementById("pfList");
        $.getJSON("http://localhost:8080/rest/proov/performances").then(function (pfDataItems) {
            var selectPfList = document.createElement("select");
            selectPfList.setAttribute("id", "pfSelect");
            divForPfList.appendChild(selectPfList);
            var pfDataOption = document.createElement("option");
            pfDataOption.setAttribute("value", "-1");
            pfDataOption.text = "";
            selectPfList.appendChild(pfDataOption);
            for (var i = 0; i < pfDataItems.length; i++) {
                var pfDataOption = document.createElement("option");
                pfDataOption.setAttribute("value", pfDataItems[i].performanceId);
                pfDataOption.text = pfDataItems[i].performanceName;
                selectPfList.appendChild(pfDataOption);
            }
        })
    }
}

function savePfData() {
    if (document.getElementById("pfSelect").value == "-1") {
        alert("Valik tegemata, naaseme ilma muutusteta");
        cleanPfTags();
    } else {
        var jsonPfData = {
            "performanceId": document.getElementById("pfSelect").value,
            "artworkId": curAwId
        };
        var jsonPfString = JSON.stringify(jsonPfData);
        $.ajax({
            url: "http://localhost:8080/rest/proov/artwork_performances",
            type: "POST",
            data: jsonPfString,
            contentType: conText,
            success: function () {
                getCurrentArtworkPerformances();
                cleanPfTags();
            }, error: function (XMLHTTPRequest, textStatus, errorThrown) {
                console.log("Error on adding data to artwork_prformances")
            }
        })
    }
}

function cleanPfTags() {
    document.getElementById("kosseisuTeade").innerHTML = "";
    document.getElementById("pfList").innerHTML = "";
    document.getElementById("pfSave").innerHTML = "";
}

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

