CREATE TABLE `artwork_performances`(
	`artwork_performances_id` INT (6) UNSIGNED NOT NULL AUTO_INCREMENT,
    `performance_id` INT(4) UNSIGNED NOT NULL,
    `artwork_id` INT(9) UNSIGNED NOT NULL,
    PRIMARY KEY (`artwork_performances_id`),
    KEY `performance_id_idx` (`performance_id`),
    KEY `ap_artwork_id_idx` (`artwork_id`),
    CONSTRAINT `artwork_id_ap` FOREIGN KEY (`artwork_id`) REFERENCES `artwork_units`(`artwork_id`) ON UPDATE NO ACTION,
    CONSTRAINT `performance_id` FOREIGN KEY (`performance_id`) REFERENCES `performances`(`performance_id`) ON UPDATE NO ACTION
);