package resources;

import dao.ArtworkUnit;
import resources.utils.DatabaseConnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ArtworkToControl {

    public static List<ArtworkUnit> getDuplicateArtworks(int idToControl) {
        List<ArtworkUnit> artWorksToReturn = new ArrayList<>();
        List<ArtworkUnit> artWorksToControl = new ArrayList<>();
        ArtworkUnit controllableArtwork = new ArtworkUnit();
        controllableArtwork = ArtworkResource.getArtworkById(idToControl);
        artWorksToControl = ArtworkResource.getAllArtworkUnits();
        for (int i = 0; i < artWorksToControl.size(); i++) {
            if (controllableArtwork.equals(artWorksToControl.get(i))){
                artWorksToReturn.add(artWorksToControl.get(i));
            }
        }
        if (artWorksToReturn.size()==0){
            artWorksToReturn.add(controllableArtwork);
        }
        return artWorksToReturn;
    }
}
