package dao;

public class SearchAnswer {
    private String answerType;
    private String answerValue;
    private String usedHtml;
    private String usedId;

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getAnswerValue() {
        return answerValue;
    }

    public void setAnswerValue(String answerValue) {
        this.answerValue = answerValue;
    }

    public String getUsedHtml() {
        return usedHtml;
    }

    public void setUsedHtml(String usedHtml) {
        this.usedHtml = usedHtml;
    }

    public String getUsedId() {
        return usedId;
    }

    public void setUsedId(String usedId) {
        this.usedId = usedId;
    }

    @Override
    public String toString() {
        return "SearchAnswer{" +
                "answerType='" + answerType + '\'' +
                ", answerValue='" + answerValue + '\'' +
                ", usedHtml='" + usedHtml + '\'' +
                ", usedId='" + usedId + '\'' +
                '}';
    }
}
