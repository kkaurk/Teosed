package resources;

import dao.ArtworkUnit;
import resources.utils.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static resources.utils.ResourcesUtils.javaDateToSqlDate;
import static resources.utils.ResourcesUtils.sqlDateToJavaDate;

public class ArtworkResource {

    public static ArtworkUnit getArtworkById(int artWorkIdAu) {
        ArtworkUnit artworkForReturn = new ArtworkUnit();
        String sqlQuery = "SELECT * FROM artwork_units WHERE artwork_id = ?";
        try (PreparedStatement artWstatement = DatabaseConnection.getConnection()
                .prepareStatement(sqlQuery)) {
            artWstatement.setInt(1, artWorkIdAu);
            ResultSet artwResults = artWstatement.executeQuery();
            while (artwResults.next()) {
                artworkForReturn.setArtworkID(artwResults.getInt("artwork_id"));
                artworkForReturn.setArtworkName(artwResults.getString("artwork_name"));
                artworkForReturn.setArtworkCreationTime(artwResults.getDate
                        ("artwork_creation_time"));
                artworkForReturn.setArtworkFirstPresentation(artwResults.getDate("first_presentation"));
                artworkForReturn.setPlaceOfFirstPresentation(artwResults.getString("place_of_first_presentatin"));
                artworkForReturn.setTypeId(artwResults.getInt("Type_ID"));
            }
        } catch (SQLException e) {
            System.out.println("ArtworkToControl / getArtworkById / - Error on getting " +
                    "data from artwork_units: " + e.getMessage());
        }
        return artworkForReturn;
    }

    public static List<ArtworkUnit> getAllArtworkUnits() {
        List<ArtworkUnit> dataListToReturn = new ArrayList<>();
        String sqlQuery = "SELECT * FROM artwork_units;";
        try (ResultSet artwStatement = DatabaseConnection.getConnection()
                .prepareStatement(sqlQuery).executeQuery();) {
            while (artwStatement.next()) {
                ArtworkUnit newArtworkUnitToList = new ArtworkUnit();
                newArtworkUnitToList.setArtworkID(artwStatement.getInt("artwork_id"));
                newArtworkUnitToList.setArtworkName(artwStatement.getString
                        ("artwork_name"));
                newArtworkUnitToList.setArtworkCreationTime(artwStatement.getDate
                        ("artwork_creation_time"));
                newArtworkUnitToList.setArtworkFirstPresentation(artwStatement.getDate
                        ("first_presentation"));
                newArtworkUnitToList.setPlaceOfFirstPresentation(artwStatement
                        .getString("place_of_first_presentatin"));
                newArtworkUnitToList.setTypeId(artwStatement.getInt("Type_ID"));
                dataListToReturn.add(newArtworkUnitToList);
            }
        } catch (SQLException e) {
            System.out.println("ArtworkToControl / getAllArtworkUnits / - Error " +
                    "getting data from artwork_units: " + e.getMessage());
        }
        return dataListToReturn;
    }


}
