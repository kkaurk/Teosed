

function searchThisData() {
    var dataOnPage=document.getElementById("dataToSearch").value;
    $.getJSON("http://localhost:8080/rest/search/" + dataOnPage, function (foundedAnswers){
        var answTableBody = "";
        if (foundedAnswers.length == 0){
            answTableBody = "<tr><td><b> Vasteid ei leitud </b></td></tr>" ;
        }
        for (var i=0; i<foundedAnswers.length; i++){
            answTableBody = answTableBody +
                "<tr><td>" + foundedAnswers[i].answerType +
                "</td><td><a href='../" + foundedAnswers[i].usedHtml + "?" + foundedAnswers[i].usedId + "'>" + foundedAnswers[i].answerValue + "</a>" +
                "</td></tr>";
        }
        document.getElementById("allAnswers").innerHTML=answTableBody;
    })
}


function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}