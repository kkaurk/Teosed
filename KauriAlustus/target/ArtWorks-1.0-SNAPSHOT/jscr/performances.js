var allPerformancesList;
var urlText = "http://localhost:8080/rest/proov/performances";
var conText = "application/json; charset=utf-8";

function getAllPerformances() {
    $.getJSON(urlText).then( function (performancesData) {
        allPerformancesList = performancesData;
            var tableBodyContent = "";
            var radioId = "";
            for (var i = 0; i < performancesData.length; i++) {
                radioId = "pfRadio" + i;
                tableBodyContent = tableBodyContent +
                    "<tr><td>" + performancesData[i].performanceName +
                    "</td><td>" + performancesData[i].performanceDescription +
                    "</td><td><input type='radio' name='pfRadio' value='" +
                    performancesData[i].performanceId + "' id='" + radioId + "'> " +
                    "</td></tr>";
            }
            document.getElementById("performanceTableBody").innerHTML = tableBodyContent;
        }
    )
}

getAllPerformances();

function saveAddedPerformance() {
    var jsonData = {
        "performanceName": document.getElementById("perfNameAdd").value,
        "performanceDescription": document.getElementById("perfDescription").value
    };
    var jsonDataString = JSON.stringify(jsonData);
    $.ajax({
        url: urlText,
        type: "POST",
        data: jsonDataString,
        contentType: conText,
        success: function () {
//            emptyDataLabels();
            getAllPerformances();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on adding new autor");
        }
    });
    document.getElementById("overlayForInput").style.display = "none";
}

function findplaceInPerfList() {
    var pfRadioName = "";
    var returnI = -1;
    for (var i = 0; i < allPerformancesList.length; i++) {
        pfRadioName = "pfRadio" + i;
        if (document.getElementById(pfRadioName).checked) {
            returnI = i;
        }
    }
    return returnI;
}

function perfDataChange() {
    var pfListPlaceNr = findplaceInPerfList();
    if (pfListPlaceNr === -1) {
        alert("Esituskoosseis on valimata");
    } else {
        document.getElementById("perfIdModify").value = allPerformancesList[pfListPlaceNr].performanceId;
        document.getElementById("perfNameModify").value = allPerformancesList[pfListPlaceNr].performanceName;
        document.getElementById("perfDescriptionModify").value = allPerformancesList[pfListPlaceNr].performanceDescription;
        document.getElementById("overlayForChange").style.display = "block";
    }
}

function perfSaveChanges() {
    var jsonPfChanges = {
        "performanceId": document.getElementById("perfIdModify").value,
        "performanceName": document.getElementById("perfNameModify").value,
        "performanceDescription": document.getElementById("perfDescriptionModify").value
    };
    var jsonPfString = JSON.stringify(jsonPfChanges);

    $.ajax({
        url: "http://localhost:8080/rest/proov/performances/performanceId",
        type: "PUT",
        data: jsonPfString,
        contentType: conText,
        success: function () {
            getAllPerformances();
            document.getElementById("overlayForChange").style.display = "none";
        }, error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("Error modifying data");
        }
    }).then(getAllPerformances())


}

function deletePerformanceData() {
    var pfListPlaceNr = findplaceInPerfList();
    if (pfListPlaceNr === -1) {
        alert("Esituskoosseis on valimata");
    } else {
        var pfDataId = allPerformancesList[pfListPlaceNr].performanceId;
        var jsonData = {
            "performanceId": pfDataId
        };
        var jsonDataString = JSON.stringify(jsonData);
        $.ajax({
            url: urlText,
            type: "DELETE",
            data: jsonDataString,
            contentType: conText,
            success: function () {
                getAllPerformances();
            },
            error: function (XMLHTTPRequest, textStatus, errorThrown) {
                console.log("Error on deleting autor data");
            }
        }).then(getAllPerformances())
    }
    //getAllAuthors();
}






function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

function inputOverlayOn() {
    document.getElementById("overlayForInput").style.display = "block";
}

function backFromAddOverlay() {
    document.getElementById("overlayForInput").style.display = "none";
}

function backFromChangeOverlay() {
    document.getElementById("overlayForChange").style.display = "none";
}

