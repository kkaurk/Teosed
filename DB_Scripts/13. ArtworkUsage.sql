CREATE TABLE `artwork_usage` (
	`artwork_usage` INT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    `parent_artwork` INT(9) UNSIGNED NOT NULL,
    `child_artwork` INT(9) UNSIGNED NOT NULL,
    PRIMARY KEY (`artwork_usage`),
    KEY `parent_artwork_idx` (`parent_artwork`),
    KEY `child_artwork_idx`(`child_artwork`),
    CONSTRAINT `parent_artwork` FOREIGN KEY (`parent_artwork`) REFERENCES `artwork_units`(`artwork_id`) ON UPDATE NO ACTION,
    CONSTRAINT `child_artwork` FOREIGN KEY (`child_artwork`) REFERENCES `artwork_units`(`artwork_id`) ON UPDATE NO ACTION
);