    
CREATE TABLE `performances`(
	`performance_id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT, 
    `performance_name` VARCHAR(20) DEFAULT NULL,
    `performance_description` VARCHAR(80) DEFAULT NULL,
    PRIMARY KEY (`performance_id`)
);