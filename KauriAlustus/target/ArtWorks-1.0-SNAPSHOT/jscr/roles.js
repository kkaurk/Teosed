var allRoleList;
var urlText = "http://localhost:8080/rest/proov/roles";
var conText = "application/json; charset=utf-8";

function getAllRoles() {
    $.getJSON(urlText, function (allRolesData) {
        allRoleList = allRolesData;
        var tableBodyContent = "";
        for (var i = 0; i < allRolesData.length; i++) {
            tableBodyContent = tableBodyContent +
                "<tr><td>" + allRolesData[i].roleId +
                "</td><td>" + allRolesData[i].roleName +
                "</td><td>" + allRolesData[i].eayCode +
                "</td><td><button type='button' onclick='deleteRoleData(" +
                allRolesData[i].roleId + ")'>Delete</button>" +
                "<button type='button' onclick='changeRoleData(" + allRolesData[i].roleId +
                ")'>Modify</button>" +
                "</td></tr>";
        }
        document.getElementById("RolesTableBody").innerHTML = tableBodyContent;
    })
}

getAllRoles();

function addRole() {
    var jsonData = {
        "roleName" : document.getElementById("newRoleName").value,
        "eayCode" : document.getElementById("newRoleEAYCode").value
    };
    var jsonDataString = JSON.stringify(jsonData);
    $.ajax({
        url: urlText,
        type: "POST",
        data: jsonDataString,
        contentType: conText,
        success: function() {
            cleanDataLabels();
            getAllRoles();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("Error on adding new role");
            alert("Error on adding new role")
        }
    })
}

function cleanDataLabels(){
    document.getElementById("newRoleName").value="";
    document.getElementById("newRoleEAYCode").value="";
    document.getElementById("modifyRoleID").value="";
    document.getElementById("modifyRoleName").value="";
    document.getElementById("modifyRoleEAYCode").value="";
}


