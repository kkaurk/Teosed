package controller;


import dao.ArtworkUnit;
import resources.ArtworkToControl;
import resources.ArtworkResource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/artworkunit")
public class ArtworkController {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ArtworkUnit> getAllArtworkUnits(){
        return ArtworkResource.getAllArtworkUnits();
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path ("/{artworkid}")
    public ArtworkUnit getArtworkById(@PathParam("artworkid") int artworkIdToGet){
        return ArtworkResource.getArtworkById(artworkIdToGet);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/control/{artworkId01}")
    public List<ArtworkUnit> controlDuplicates(@PathParam("artworkId01")int
                                                           artworkToControl){
        return ArtworkToControl.getDuplicateArtworks(artworkToControl);
    }

}
