var curAuthId;


function getCurrentAuthorId() {
    var parts = document.URL.split("?");
    curAuthId = parts[1];
}

getCurrentAuthorId();

function getAuthorData() {
    $.getJSON("http://localhost:8080/rest/proov/author/authorId/" +
        curAuthId, function (currentAuthorData) {
        var curAuthorTableBody = "";
        for (var i=0; i<currentAuthorData.length; i++){
            curAuthorTableBody = curAuthorTableBody +
                "<tr><td>" + currentAuthorData[i].firstName +
                "</td><td>" + currentAuthorData[i].lastName +
                "</td></tr>";
        }
        document.getElementById("authorArtworksTable").innerHTML=curAuthorTableBody;
    })
}

getAuthorData();

function getAuthorArtworks() {
    $.getJSON("http://localhost:8080/rest/proov/author_artworks/authorIdV/" + curAuthId).then(function (currentAuthorArtworks) {
        if (currentAuthorArtworks.length !==0){
            var curAuthAwTable = "";
            for (var i=0; i<currentAuthorArtworks.length; i++){
                curAuthAwTable = curAuthAwTable +
                    "<tr><td><a href= '../artworkdetails.html?" + currentAuthorArtworks[i].artworkIdVV + "'>" +
                    currentAuthorArtworks[i].artworkNameV + "</a>" +
                    "</td><td>" + currentAuthorArtworks[i].artworkCreationTimeV +
                    "</td><td>" + currentAuthorArtworks[i].	typeNameV +
                    "</td><td>" + currentAuthorArtworks[i].roleNameV +
                    "</td></tr>";
            }
            document.getElementById("allAuthorArtworks").innerHTML=curAuthAwTable;
        }
    });
}

getAuthorArtworks();


function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}