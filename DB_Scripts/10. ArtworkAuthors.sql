CREATE TABLE `artwork_authors` (
	`artwork_authors_id` INT(7) UNSIGNED NOT NULL AUTO_INCREMENT,
    `artwork_id` INT(9) UNSIGNED NOT NULL,
    `author_id` INT(7) UNSIGNED NOT NULL,
    `role_id` INT(6) UNSIGNED NOT NULL,
    PRIMARY KEY (`artwork_authors_id`),
    KEY `artwork_id_idx` (`artwork_id`),
    KEY `author_id_idx` (`author_id`),
    KEY `role_id_idx` (`role_id`),
    CONSTRAINT `artwork_id_au` FOREIGN KEY (`artwork_id`) REFERENCES `artwork_units`(`artwork_id`) ON UPDATE NO ACTION,
    CONSTRAINT `author_id_au` FOREIGN KEY (`author_id`) REFERENCES `author`(`author_id`) ON UPDATE NO ACTION,
	CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `roles`(`role_id`) ON UPDATE NO ACTION