CREATE TABLE `roles` (
	`role_id`INT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
	`role_name` VARCHAR(30),
    `eay_code` VARCHAR(4),
    PRIMARY KEY (`role_id`),
    UNIQUE KEY `role_id_UNIQUE`(`role_id`)
    );