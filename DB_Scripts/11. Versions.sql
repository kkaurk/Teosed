CREATE TABLE `versions`(
	`version_id` INT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    `first_version_id` INT(9) UNSIGNED NOT NULL,
    `new_version_id` INT(9) UNSIGNED NOT NULL,
    PRIMARY KEY (`version_id`),
    KEY `first_version_id_idx` (`first_version_id`),
    KEY `new_version_id_idx` (`new_version_id`),
    CONSTRAINT `first_version_id` FOREIGN KEY (`first_version_id`) REFERENCES `artwork_units`(`artwork_id`) ON UPDATE NO ACTION,
    CONSTRAINT `new_version_id` FOREIGN KEY (`new_version_id`) REFERENCES `artwork_units`(`artwork_id`) ON UPDATE NO ACTION
);

