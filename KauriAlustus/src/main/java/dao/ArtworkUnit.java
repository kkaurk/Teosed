package dao;

import java.util.Date;
import java.util.Objects;

public class ArtworkUnit implements Comparable<ArtworkUnit> {
    private int artworkID;
    private String artworkName;
    private Date artworkCreationTime;
    private Date artworkFirstPresentation;
    private String placeOfFirstPresentation;
    private int typeId;

    public int getArtworkID() {
        return artworkID;
    }

    public void setArtworkID(int artworkID) {
        this.artworkID = artworkID;
    }

    public String getArtworkName() {
        return artworkName;
    }

    public void setArtworkName(String artworkName) {
        this.artworkName = artworkName;
    }

    public Date getArtworkCreationTime() {
        return artworkCreationTime;
    }

    public void setArtworkCreationTime(Date artworkCreationTime) {
        this.artworkCreationTime = artworkCreationTime;
    }

    public Date getArtworkFirstPresentation() {
        return artworkFirstPresentation;
    }

    public void setArtworkFirstPresentation(Date artworkFirstPresentation) {
        this.artworkFirstPresentation = artworkFirstPresentation;
    }

    public String getPlaceOfFirstPresentation() {
        return placeOfFirstPresentation;
    }

    public void setPlaceOfFirstPresentation(String placeOfFirstPresentation) {
        this.placeOfFirstPresentation = placeOfFirstPresentation;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    @Override
    public boolean equals(Object secondObject) {
        if (this == secondObject) {
            return true;
        }
        if (secondObject == null) {
            return false;
        }
        if (getClass() != secondObject.getClass()) {
            return false;
        }
        final ArtworkUnit secondArtwork = (ArtworkUnit) secondObject;
        return this.getArtworkName().equals(secondArtwork.getArtworkName())
                && this.getArtworkCreationTime().equals(secondArtwork
                .getArtworkCreationTime())
                && this.getTypeId() == secondArtwork.getTypeId()
                && this.getArtworkFirstPresentation().equals(secondArtwork
                .getArtworkFirstPresentation());
    }

    @Override
    public int hashCode() {
        int hash = 53;
        hash = hash * 47 + Objects.hashCode(typeId);
        hash = hash * 11 + Objects.hashCode(artworkName);
        hash = hash * 17 + Objects.hashCode(getArtworkCreationTime());
        return hash;
    }

    @Override
    public int compareTo(ArtworkUnit secondArtWork) {
        if (this.equals(secondArtWork)) {
            return 0;
        } else if (this.typeId < secondArtWork.getTypeId()) {
            return -1;
        } else if ((this.artworkName.compareTo(secondArtWork.getArtworkName()) < 0)) {
            return -1;
        } else if ((this.artworkCreationTime.compareTo(secondArtWork
                .getArtworkCreationTime())) < 0) {
            return -1;
        }
        return 1;
    }

    @Override
    public String toString() {
        return "ArtworkUnit{" +
                "artworkID=" + artworkID +
                ", artworkName='" + artworkName + '\'' +
                ", artworkCreationTime=" + artworkCreationTime +
                ", artworkFirstPresentation=" + artworkFirstPresentation +
                ", placeOfFirstPresentation='" + placeOfFirstPresentation + '\'' +
                ", typeId=" + typeId +
                '}';
    }
}
