CREATE TABLE `artwork_texts`(
	`text_id` INT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    `artwork_id` int(9) NOT NULL,
    `texts_text` VARCHAR(20000),
    `publish_all` TINYINT NOT NULL,
    `publish_participant` TINYINT NOT NULL,
    `original_language` VARCHAR(40),
    `translate_type_id` INT(1),
    PRIMARY KEY (`text_id`),
    KEY `translate_type_id_idx`(`translate_type_id`),
    CONSTRAINT `translate_type_id` FOREIGN KEY (`translate_type_id`) REFERENCES `translate_type` (`translate_type_id`) ON UPDATE NO ACTION    
);