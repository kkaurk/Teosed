package controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import resources.UniversalResource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;


@Path("/proov")
public class UniversalController {


    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{tblName}")
    public static String getData(@PathParam("tblName") String tableName) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(UniversalResource.getAllData(tableName));
        return jsonString;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{tblName}/{fldToFind}/{valToFind}")
    public static String getDataById(@PathParam("tblName") String tableName,
                                     @PathParam("fldToFind") String fieldToFind,
                                     @PathParam("valToFind") String valueToFind){
        Gson gson = new Gson();
        String jsonString  =gson.toJson(UniversalResource.getDataFromTableById
                (tableName, fieldToFind,valueToFind));
        return jsonString;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{tblName}")
    public static String addNewDataToDb(@PathParam("tblName") String
                                                tableName, String dataAddMap) {
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<LinkedHashMap<String, Object>>() {
        }
                .getType();
        Map<String, Object> receivadDataMap = gson.fromJson(String.valueOf(dataAddMap),
                stringStringMap);
        return UniversalResource.addDataToTable(receivadDataMap, tableName);

    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{tblName}")
    public static boolean deleteDataFromDb(@PathParam("tblName") String tableName, String
            dataDeleteMap) {
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<LinkedHashMap<String, String>>() {
        }.getType();
        Map<String, String> receivadDataMap = gson.fromJson(String.valueOf(dataDeleteMap),
                stringStringMap);
        return UniversalResource.deleteDataById(tableName, receivadDataMap);

    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{tblName}/{idAlias}")
    public boolean updateDataInTable(
            @PathParam("tblName") String tableName,
            @PathParam("idAlias") String idName,
            String updateDataMap) {
        Gson gson = new Gson();
        Type stringStringMap = new TypeToken<LinkedHashMap<String, String>>() {
        }.getType();
        Map<String, String> receivadDataMap = gson.fromJson(String.valueOf(updateDataMap),
                stringStringMap);
        return UniversalResource.updateDataInTable(receivadDataMap, tableName, idName);
    }


}
