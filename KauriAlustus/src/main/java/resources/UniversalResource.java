package resources;

import com.mysql.cj.api.mysqla.result.Resultset;
import org.apache.commons.lang.builder.ToStringBuilder;
import resources.utils.DatabaseConnection;
import resources.utils.ResourcesUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public abstract class UniversalResource {

    public static List<Map<String, String>> getAllData(String myDbName) {
        List<Map<String, String>> sqlAnswers = new ArrayList();
        Map<String, String> tblStructsMy = new LinkedHashMap<>();
        tblStructsMy = getTableFieldsAndAlias(myDbName);
        String sqlQuery = "SELECT * FROM " + myDbName;
        try (ResultSet answerResults = DatabaseConnection.getConnection()
                .prepareStatement(sqlQuery).executeQuery();) {
            Map<String, Integer> tblDataTypes = new HashMap<>();
            for (int myKey = 1; myKey <= answerResults.getMetaData().getColumnCount();
                 myKey++) {
                tblDataTypes.put(answerResults.getMetaData().getColumnName(myKey),
                        answerResults.getMetaData().getColumnType(myKey));
            }
            while (answerResults.next()) {
                Map<String, String> newRowInRS = new LinkedHashMap<>();
                for (String key : tblStructsMy.keySet()) {
                    if (tblDataTypes.get(tblStructsMy.get(key)) == 91) {
                        newRowInRS.put(key, ResourcesUtils.sqlDateToString
                                (answerResults.getDate
                                        (tblStructsMy.get
                                                (key))));
                    } else {
                        newRowInRS.put(key, answerResults.getString(tblStructsMy.get
                                (key)));
                    }
                }
                sqlAnswers.add(newRowInRS);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting data from table " + myDbName + e
                    .getMessage());
        }
        return sqlAnswers;

    }

    public static List<Map<String, String>> getDataFromTableById(String myTblName,
                                                                 String findField,
                                                                 String findData) {
        List<Map<String, String>> dataFromTable = new ArrayList<>();
        Map<String, String> tblStructsMy = new LinkedHashMap<>();
        tblStructsMy = getTableFieldsAndAlias(myTblName);
        String sqlQuery = "SELECT * FROM " + myTblName + " WHERE " + tblStructsMy.get
                (findField) + "=?";
        try (PreparedStatement dataFromTableById = DatabaseConnection.getConnection()
                .prepareStatement(sqlQuery)) {
            dataFromTableById.setString(1, findData);
            ResultSet dataReceived = dataFromTableById.executeQuery();
            Map<String, Integer> tblDataTypes = new HashMap<>();
            for (int myKey = 1; myKey <= dataReceived.getMetaData().getColumnCount();
                 myKey++) {
                tblDataTypes.put(dataReceived.getMetaData().getColumnName(myKey),
                        dataReceived.getMetaData().getColumnType(myKey));
            }
            while (dataReceived.next()) {
                Map<String, String> dataRow = new LinkedHashMap<>();
                for (String key : tblStructsMy.keySet()) {
                    if (tblDataTypes.get(tblStructsMy.get(key)) == 91) {
                        dataRow.put(key, ResourcesUtils.sqlDateToString(dataReceived
                                .getDate(tblStructsMy.get(key))));
                    } else {
                        dataRow.put(key, dataReceived.getString(tblStructsMy.get(key)));
                    }
                }
                dataFromTable.add(dataRow);
            }
        } catch (SQLException e) {
            System.out.println("Error on getting data from table " + myTblName + ": " + e
                    .getMessage());
        }


        return dataFromTable;
    }

    public static String addDataToTable(Map<String,
            Object> newDataToAdd, String myDbName) {
        Map<String, String> fieldsAliases = getTableFieldsAndAlias(myDbName);
        Boolean second = false;
        int newKey = 0;
        List<Object> dataHold = new ArrayList<>();
        String sqlFieldsInQuery = "(`";
        String sqlValuesInQuery = "(";
        for (String key : newDataToAdd.keySet()) {
            if (second == true) {
                sqlFieldsInQuery = sqlFieldsInQuery + ", `";
                sqlValuesInQuery = sqlValuesInQuery + ", ";
            }
            sqlFieldsInQuery = sqlFieldsInQuery + fieldsAliases.get(key)
                    + "`";
            sqlValuesInQuery = sqlValuesInQuery + "?";
            dataHold.add(newDataToAdd.get(key));
            second = true;
        }
        sqlFieldsInQuery = sqlFieldsInQuery + ")";
        sqlValuesInQuery = sqlValuesInQuery + ")";
        String sqlQuery = "INSERT INTO `" + myDbName + "` " + sqlFieldsInQuery
                + " VALUES " + sqlValuesInQuery + ";";
        int suva = 0;
        try (PreparedStatement addDataStatement = DatabaseConnection.getConnection()
                .prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
            for (int myCounter = 0; myCounter < dataHold.size(); myCounter++) {
                addDataStatement.setObject((myCounter + 1), dataHold.get((myCounter)));
            }
            int addDataResult = addDataStatement.executeUpdate();
            suva = addDataResult;
        } catch (SQLException e) {
            System.out.println("Error on adding new data to " + myDbName + ":" +
                    " " + e.getMessage());
        }
        return String.valueOf(suva);
    }

    public static boolean deleteDataById(String myDbName, Map<String, String> dataToDelete) {
        boolean isDataDeleted = false;
        Map<String, String> fieldsAliases = getTableFieldsAndAlias(myDbName);
        int deleteId = 0;
        String deleteField = "";
        for (String key : dataToDelete.keySet()) {
            deleteId = Integer.parseInt(dataToDelete.get(key));
            deleteField = fieldsAliases.get(key);
        }
        String sqlQuery = "DELETE FROM " + myDbName + " WHERE `" + deleteField + "` = " +
                deleteId + ";";
        try (Statement deleteStatement = DatabaseConnection.getConnection().prepareStatement
                (sqlQuery);) {
            Integer resultCode = deleteStatement.executeUpdate(sqlQuery, Statement
                    .RETURN_GENERATED_KEYS);
            if (resultCode == 1) {
                isDataDeleted = true;
            }
        } catch (SQLException e) {
            System.out.println("Error on deleteing data from " + myDbName);
        }
        return isDataDeleted;
    }

    public static boolean updateDataInTable(Map<String, String> dataToUpdate,
                                            String myTableName,
                                            String idColumnName) {
        boolean isDataUpdated = false;
        Map<String, String> fieldAliases = getTableFieldsAndAlias((myTableName));
        String sqlUpdates = "";
        String sqlWhere = "";
        boolean second = false;
        for (String key : dataToUpdate.keySet()) {
            if (key.equals(idColumnName)) {
                sqlWhere = sqlWhere + "`" + fieldAliases.get(key) + "` = " + dataToUpdate.get(key)
                        + ";";
            } else {
                if (second == true) {
                    sqlUpdates = sqlUpdates + ", ";
                }
                sqlUpdates = sqlUpdates + "`" + fieldAliases.get(key) + "` = '" + dataToUpdate
                        .get(key) + "'";
                second = true;
            }
        }
        String sqlQuery = "UPDATE " + myTableName + " SET " + sqlUpdates + " WHERE " + sqlWhere +
                ";";
        try (Statement dataUpdateStatement = DatabaseConnection.getConnection().prepareStatement
                (sqlQuery);) {
            Integer resultCode = dataUpdateStatement.executeUpdate(sqlQuery);
            if (resultCode == 1) {
                isDataUpdated = true;
            }
        } catch (SQLException e) {
            System.out.println("Eroor on updating data in table" + myTableName + ", " + e
                    .getMessage());
        }
        return isDataUpdated;
    }


    public static Map<String, String> getTableFieldsAndAlias(String myTblName) {
        Map<String, String> tblStructs = new LinkedHashMap<>();
        String dbSqlQuery = "SELECT * FROM w_structses WHERE table_name = '" +
                myTblName + "';";
        try (ResultSet dbAnswerResults = DatabaseConnection.getConnection()
                .prepareStatement(dbSqlQuery).executeQuery();) {
            while (dbAnswerResults.next()) {
                tblStructs.put(dbAnswerResults.getString("field_alias"),
                        dbAnswerResults.getString("field_name"));
            }
        } catch (SQLException e) {
            System.out.println("Error on getting tabeli struktuur" + e.getMessage());
        }
        return tblStructs;
    }


}
