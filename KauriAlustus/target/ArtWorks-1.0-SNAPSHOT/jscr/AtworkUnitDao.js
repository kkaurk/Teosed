var allArtworUnitkList;
var urlTextArtworkUnits = "http://localhost:8080/rest/proov/artworkunit";
var conText = "application/json; charset=utf-8";

function getAllArtworkUnits() {
    $.getJSON("http://localhost:8080/rest/artworkunit", function (allArtworkUnitData) {
        allArtworUnitkList = allArtworkUnitData;
        var artworksTableBody = "";
        var radioId = "";
        for (var i = 0; i < allArtworkUnitData.length; i++) {
            var myCrDateStr1 = allArtworkUnitData[i].artworkCreationTime.split("T");
            var myCrDateStr2 = myCrDateStr1[0];
            myCrDateStr1 = myCrDateStr2.split("-");
            myCrDateStr2 = myCrDateStr1[2] + "." + myCrDateStr1[1] + "." + myCrDateStr1[0];

            var myCrDateStr3 = allArtworkUnitData[i].artworkFirstPresentation.split("T");
            var myCrDateStr4 = myCrDateStr3[0];
            myCrDateStr3 = myCrDateStr4.split("-");
            myCrDateStr4 = myCrDateStr3[2] + "." + myCrDateStr3[1] + "." + myCrDateStr3[0];
            radioId = "awradio" + i;
            artworksTableBody = artworksTableBody +
                "<tr><td>" + allArtworkUnitData[i].artworkID +
                "</td><td>" + allArtworkUnitData[i].artworkName +
                "</td><td>" + myCrDateStr2 +
                "</td><td>" + myCrDateStr4 +
                "</td><td>" + allArtworkUnitData[i].placeOfFirstPresentation +
                "</td><td>" + allArtworkUnitData[i].typeId +
                "</td><td><input type='radio' name='awUnitId' value='" + allArtworkUnitData[i].artworkID + "' id='" + radioId + "'" +
                "</td></tr>";
        }
        document.getElementById("TeosteDataDao").innerHTML = artworksTableBody;
    })
}

getAllArtworkUnits();

function ControlData() {
    var txtMy = "";
    var awRadioName = "";
    var idToControl;
    var isChecked = false;
    for (var i = 0; i < allArtworUnitkList.length; i++) {
        awRadioName = "awradio" + i;
        if (document.getElementById(awRadioName).checked) {
            idToControl = document.getElementById(awRadioName).value;
            txtMy = txtMy + idToControl + " ;";
            isChecked = true;
        }
    }
    if (isChecked == false){
        alert("Kontrollitav teos on valimata")
    }
    var urlHelpString = "http://localhost:8080/rest/artworkunit/control/" + idToControl;
    $.getJSON(urlHelpString, function (controlledArtworkUnitData) {
        var artworksTableBody = "";
        var radioId = "";
        for (var i = 0; i < controlledArtworkUnitData.length; i++) {
            var myCrDateStr1 = controlledArtworkUnitData[i].artworkCreationTime.split("T");
            var myCrDateStr2 = myCrDateStr1[0];
            myCrDateStr1 = myCrDateStr2.split("-");
            myCrDateStr2 = myCrDateStr1[2] + "." + myCrDateStr1[1] + "." + myCrDateStr1[0];

            var myCrDateStr3 = controlledArtworkUnitData[i].artworkFirstPresentation.split("T");
            var myCrDateStr4 = myCrDateStr3[0];
            myCrDateStr3 = myCrDateStr4.split("-");
            myCrDateStr4 = myCrDateStr3[2] + "." + myCrDateStr3[1] + "." + myCrDateStr3[0];
            radioId = "awradio" + i;
            artworksTableBody = artworksTableBody +
                "<tr><td>" + controlledArtworkUnitData[i].artworkID +
                "</td><td>" + controlledArtworkUnitData[i].artworkName +
                "</td><td>" + myCrDateStr2 +
                "</td><td>" + myCrDateStr4 +
                "</td><td>" + controlledArtworkUnitData[i].placeOfFirstPresentation +
                "</td><td>" + controlledArtworkUnitData[i].typeId +
                "</td></tr>";
        }
        document.getElementById("TeosteDataDao").innerHTML = artworksTableBody;
    })

}

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}