var allAuthorsList;
var urlText = "http://localhost:8080/rest/proov/author";
var conText = "application/json; charset=utf-8";

function getAllAuthors() {
    $.getJSON(urlText).then( function (allAuthorData) {
            allAuthorsList = allAuthorData;
            var tableBodyContent = "";
            var radioId = "";
            for (var i = 0; i < allAuthorData.length; i++) {
                radioId = "auRadio" + i;
                tableBodyContent = tableBodyContent +
                    "<tr><td><a href='../authordetails.html?" + allAuthorData[i].authorId + "'>" + allAuthorData[i].firstName + "</a>" +
                    "</td><td><a href='../authordetails.html?" + allAuthorData[i].authorId + "'>" + allAuthorData[i].lastName + "</a>" +
                    "</td><td><input type='radio' name='auRadio' value='" +
                    allAuthorData[i].authorId + "' id='" + radioId + "'> " +
                    "</td></tr>";
            }
            document.getElementById("authorsTableBody").innerHTML = tableBodyContent;
        }
    )
}

getAllAuthors();


function saveAddedAuthor() {
    var jsonData = {
        "firstName": document.getElementById("auFirstNameAdd").value,
        "lastName": document.getElementById("auLastNameAdd").value
    };
    var jsonDataString = JSON.stringify(jsonData);
    $.ajax({
        url: urlText,
        type: "POST",
        data: jsonDataString,
        contentType: conText,
        success: function () {
            emptyDataLabels();
            getAllAuthors();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("error on adding new autor");
        }
    });
    document.getElementById("overlayForInput").style.display = "none";
}

function findplaceInList() {
    var auRadioName = "";
    var returnI = -1;
    for (var i = 0; i < allAuthorsList.length; i++) {
        auRadioName = "auRadio" + i;
        if (document.getElementById(auRadioName).checked) {
            returnI = i;
        }
    }
    return returnI;
}

function auDataChange() {
    var listPlaceNr = findplaceInList();
    if (listPlaceNr === -1) {
        alert("Autor on valimata");
    } else {
        document.getElementById("auIdModify").value = allAuthorsList[listPlaceNr].authorId;
        document.getElementById("auFirstNameModify").value = allAuthorsList[listPlaceNr].firstName;
        document.getElementById("auLastNameModify").value = allAuthorsList[listPlaceNr].lastName;
        document.getElementById("overlayForChange").style.display = "block";
    }
}

function auSaveChanges() {
    var jsonAuChanges = {
        "authorId": document.getElementById("auIdModify").value,
        "firstName": document.getElementById("auFirstNameModify").value,
        "lastName": document.getElementById("auLastNameModify").value
    };
    var jsonAuString = JSON.stringify(jsonAuChanges);
    $.ajax({
        url: "http://localhost:8080/rest/proov/author/authorId",
        type: "PUT",
        data: jsonAuString,
        contentType: conText,
        success: function () {
            getAllAuthors();
        }, error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("Error modifying data");
        }
    });
    document.getElementById("overlayForChange").style.display = "none";
}

function deleteAuthorsData() {
    var listPlaceNr = findplaceInList();
    if (listPlaceNr === -1) {
        alert("Autor on valimata");
    } else {
        var authorDataId = allAuthorsList[listPlaceNr].authorId;
    }
    var jsonData = {
        "authorId": authorDataId
    };
    var jsonDataString = JSON.stringify(jsonData);
    $.ajax({
        url: urlText,
        type: "DELETE",
        data: jsonDataString,
        contentType: conText,
        success: function () {
            emptyDataLabels();
            getAllAuthors();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("Error on deleting autor data");
        }
    })
}




function emptyDataLabels() {
    document.getElementById("auFirstNameAdd").value = "";
    document.getElementById("auLastNameAdd").value = "";
    document.getElementById("auFirstNameModify").value = "";
    document.getElementById("auLastNameModify").value = "";
}


function backFromAddOverlay() {
    document.getElementById("overlayForInput").style.display = "none";
    emptyDataLabels();
}


function inputOverlayOn() {
    document.getElementById("overlayForInput").style.display = "block";
}

function backFromChangeOverlay() {
    emptyDataLabels();
    document.getElementById("overlayForChange").style.display = "none";
}

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}