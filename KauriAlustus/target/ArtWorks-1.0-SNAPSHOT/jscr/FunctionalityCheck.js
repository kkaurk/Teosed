function getArtworkUthors() {
    // järgmine muutuja peab tulema ette funktsiooni sisendist
    var awIdToGetAuthors = 5;
    var urlStringToGetAuthors = "http://localhost:8080/rest/proov/artwork_authors_view";
    urlStringToGetAuthors = urlStringToGetAuthors + "/artworkIdV/" + awIdToGetAuthors;
    $.getJSON(urlStringToGetAuthors, function (awAuthorsData) {
        var retString = "<p>";
        var second = false;
        for (var i=0; i<awAuthorsData.length; i++){
            if (second === true){
                retString = retString + "<br>";
            }
            retString = retString + awAuthorsData[i].roleNameV + ": " + awAuthorsData[i].firstNameV + " " +
                    awAuthorsData[i].lastNameV;
            second = true;
        }
        retString = retString + "</p>";
        return retString;
    })
}