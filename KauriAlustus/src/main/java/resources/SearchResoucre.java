package resources;

import dao.SearchAnswer;
import resources.utils.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SearchResoucre {

    public static List<SearchAnswer> getAllAnswers(String searchValue){
        List<SearchAnswer> allAnswers = new ArrayList<>();
        String sqlQuery;
        String answType;
        String answHtml;
        int nrOfStatements;
        for (int i=1; i<3; i++) {
            if (i==1) {
                sqlQuery = "SELECT `artwork_name` AS `answer`, `artwork_id` AS `id` FROM" +
                        " `artwork_units` WHERE `artwork_name` LIKE ?";
                answType="Teos";
                answHtml="artworkdetails.html";
                nrOfStatements=1;
            } else {
                sqlQuery = "SELECT CONCAT(`first_name`,' ', `last_name`) AS " +
                        "`answer`, `author_id` AS `id` FROM `author` WHERE `first_name`" +
                        " LIKE ? OR `last_name` LIKE ?;";
                answType="Autor";
                answHtml="authordetails.html";
                nrOfStatements=2;
            }
            try (PreparedStatement searchStatement = DatabaseConnection.getConnection()
                    .prepareStatement(sqlQuery)) {
                for (int j=1; j<=nrOfStatements; j++) {
                    searchStatement.setString(j, "%" + searchValue + "%");
                }
                ResultSet searchReceived = searchStatement.executeQuery();
                while (searchReceived.next()) {
                    SearchAnswer newSearchAnswer = new SearchAnswer();
                    newSearchAnswer.setAnswerType(answType);
                    newSearchAnswer.setAnswerValue(searchReceived.getString("answer"));
                    newSearchAnswer.setUsedHtml(answHtml);
                    newSearchAnswer.setUsedId(searchReceived.getString("id"));
                    allAnswers.add(newSearchAnswer);
                }
            } catch (SQLException e) {
                System.out.println("Error on getting search answer from artwork_units: " +
                        e.getMessage());
            }
        }
        return allAnswers;
    }

}
