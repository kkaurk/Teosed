CREATE TABLE `artwork_types` (
  `type_id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_name` varchar(30) NOT NULL,
  `type_description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `type_id_UNIQUE` (`type_id`),
  UNIQUE KEY `type_name_UNIQUE` (`type_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Types of artwork - music, theatre .....';