CREATE TABLE `music_data`(
	`music_id` INT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    `artwork_id` INT(9) NOT NULL,
    `length` TIME,
    `other_data` VARCHAR(60),
    PRIMARY KEY (`music_id`),
    KEY `artwork_id_idx` (`artwork_id`),
    CONSTRAINT `artwork_id`FOREIGN KEY (`artwork_id`) REFERENCES `artwork_units`(`artwork_id`) ON UPDATE NO ACTION
);