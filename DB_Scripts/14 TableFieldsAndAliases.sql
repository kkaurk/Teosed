CREATE TABLE `w_structses` (
  `struct_id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(40) NOT NULL,
  `field_name` varchar(60) NOT NULL,
  `field_alias` varchar(60) NOT NULL,
  PRIMARY KEY (`struct_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;