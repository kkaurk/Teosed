var allArtworkList;
var urlTextArtworks = "http://localhost:8080/rest/proov/artwork_units";
var conText = "application/json; charset=utf-8";

function hello() {
    alert("Hello");
}

hello();

$(document).ready(function () {
    $("#newArtworkCreation").datepicker({dateFormat: "dd.mm.yy"});
    $("#newArtworkPresentation").datepicker({dateFormat: "dd.mm.yy"});
});


function getAllArtworks() {
    $.getJSON("http://localhost:8080/rest/proov/artworkunits_view", function (allArtworksData) {
        allArtworkList = allArtworksData;
        var artworksTableBody = "";
        for (var i = 0; i < allArtworksData.length; i++) {

            artworksTableBody = artworksTableBody +
                "<tr><td>" + allArtworksData[i].artworkIdV +
                "</td><td>" + allArtworksData[i].artworkNameV +
                "</td><td>" + allArtworksData[i].artworkTimeV +
                "</td><td>" + allArtworksData[i].artworkPresentationV +
                "</td><td>" + allArtworksData[i].artworkPresentatinV +
                "</td><td>" + allArtworksData[i].artworkTypeNameV +
                "</td><td><button type='button' onclick='deleteArtwork(" +
                allArtworksData[i].artworkID + ")'>Delete</button>" +
                "<button type='button'" + " onclick='changeArtworkData(" +
                allArtworksData[i].artworkID + ")'>Modify</button>" +
                "</td></tr>";
        }
        document.getElementById("TeosteData").innerHTML = artworksTableBody;
    })
}

function typesOfArtworks() {
    var myDiv = document.getElementById("artWorkTypesList");
    $.getJSON("http://localhost:8080/rest/proov/artwork_types", function (allArtworkTypesForList) {
        var selectList = document.createElement("select");
        selectList.setAttribute("id", "mySelect");
        myDiv.appendChild(selectList);
        for (var i = 0; i < allArtworkTypesForList.length; i++) {
            var stylesOption = document.createElement("option");
            stylesOption.setAttribute("value", allArtworkTypesForList[i].typeId);
            stylesOption.text = allArtworkTypesForList[i].typeName;
            selectList.appendChild(stylesOption);
        }
    })
}

getAllArtworks();
typesOfArtworks();

function addArtworkUnit() {
    var parts1 = document.getElementById("newArtworkCreation").value.split('.');
    var firstDate = parts1[2] + "/" + parts1[1] + "/" + parts1[0];
    var parts2 = document.getElementById("newArtworkPresentation").value.split('.');
    var secondDate = parts2[2] + "/" + parts2[1] + "/" + parts2[0];
    var listElementValue1 = document.getElementById("mySelect");
    alert(listElementValue1);
    var listElementValue2 = listElementValue1.value;
    alert(listElementValue2);
    var jsonDataAdd = {
        "artworkName": document.getElementById("newArtworkName").value,
        "artworkCreationTime": firstDate,
        "artworkFirstPresentation": secondDate,
        "placeOfFirstPresentation": document.getElementById("newArtworkPresentPlace").value,
        "typeId": listElementValue2
    };
    var jsonDataAddString = JSON.stringify(jsonDataAdd);
    $.ajax({
        url: urlTextArtworks,
        type: "POST",
        data: jsonDataAddString,
        contentType: conText,
        success: function () {
            alert("Andmed sisestatud");
            getAllArtworks();
            emptyDataLabels();
        },
        error: function (XMLHTTPRequest, textStatus, errorThrown) {
            console.log("Eroor on adding artwork");
        }
    })
}


function emptyDataLabels() {
    document.getElementById("newArtworkName").value = "";
    document.getElementById("newArtworkCreation").value = "";
    document.getElementById("newArtworkPresentation").value = "";
    document.getElementById("newArtworkPresentPlace").value = "";
    document.getElementById("newArtworkType").value = "";
    document.getElementById("modifyArtworId").value = "";
    document.getElementById("modifyArtworkName").value = "";
    document.getElementById("modifyArtworkCreationTime").value = "";
    document.getElementById("modifyArtworkPresentation").value = "";
    document.getElementById("modifyArtworkPresentPlace").value = "";
    document.getElementById("modifyArtworkType").value = "";
}



